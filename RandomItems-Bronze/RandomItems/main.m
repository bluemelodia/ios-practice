//
//  main.m
//  RandomItems
//
//  Created by Melanie H on 5/19/15.
//  Copyright (c) 2015 Melanie Hsu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MLHItem.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        NSMutableArray *array = [[NSMutableArray alloc] init]; //create an instance of NSMutableArray
        /* Send the message addObject: to the NSMutableArray pointed
         to by the variable array, passing an instance of a NSString each time */
        for (int i = 0; i < 10; i++) {
            array[i] = [MLHItem randomItem];
        }
        
        /* Iterate over the array, access each string, and output it to the console. */
        for (MLHItem *item in array) {
            NSLog(@"%@", item);
        }
        NSLog(@"%@", array[[array count]]);
        array = nil; //destroy the array object
    }
    return 0;
}

/*  Instantiating a NSString outside of an array
    NSString *myString = @"Hello, World!"; */