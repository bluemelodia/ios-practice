//
//  MLHItem.m
//  RandomItems
//
//  Created by Melanie H on 5/19/15.
//  Copyright (c) 2015 Melanie Hsu. All rights reserved.
//

/* The implementation file, containing code for methods that the class implements. */

#import "MLHItem.h"

@implementation MLHItem

/* because randomItem is a class method, self refers to the MLHItem class itself instead of an instance;
   class methods should use self in convenience methods instead of their class name so that a subclass
   can be sent the same message -> this is a convenience method because it returns an obj of their type */
+ (instancetype)randomItem {
    //create an immutable array of three adjectives - only use below syntax for immutable arrays
    NSArray *adjectives = @[@"Sensational", @"Explosive", @"Riveting", @"Derogatory", @"Villainous"];
    //create an immutable array of three nouns
    NSArray *nouns = @[@"MacBook", @"Potato", @"Sandal", @"Toaster", @"Diarrhea"];
    //get the index of a random noun and adjective from the list
    NSInteger adjectiveIndex = arc4random()%[adjectives count];
    NSInteger nounIndex = arc4random()%[nouns count];
    NSString *randomString = [NSString stringWithFormat:@"%@ %@", adjectives[adjectiveIndex], nouns[nounIndex]]; /* stringWithFormat: is sent to the NSString class, returning an NSString instance with the passed-in parameters */
    int randomValue = arc4random()%100;
    NSString *randomSerialNumber = [NSString stringWithFormat:@"%c%c%c%c%c",
                                    '0' + arc4random()%10,
                                    'A' + arc4random()%26,
                                    '0' + arc4random()%10,
                                    'A' + arc4random()%26,
                                    '0' + arc4random()%10];
    MLHItem *newItem = [[self alloc] initWithItemName:randomString
                                       valueInDollars:randomValue
                                         serialNumber:randomSerialNumber];
    return newItem;
}

- (instancetype)initWithItemName:(NSString *)name
                valueInDollars:(int)value
                serialNumber:(NSString *)sNumber {
    /* call the superclass's designated initializer;
     this is the first thing you always do in the designated initailizer */
    self = [super init];
    
    /* did the superclass's designated initializer succeed? */
    if (self) {
        /* set the instance vars directly in the initializer, instead of using accessors */
        _itemName = name;
        _valueInDollars = value;
        _serialNumber = sNumber;
        /* set date created to the current time */
        _dateCreated = [[NSDate alloc] init];
    }
    /* return the address of the newly initialized object; returns nil on failure */
    return self;
}

/* calls the designated initializer, passing along the name and default values for the other params */
- (instancetype)initWithItemName:(NSString *)name {
    return [self initWithItemName:name valueInDollars:0 serialNumber:@""];
}

/* must override the init method inherited from NSObject and link it to our designated initailizer */
- (instancetype)init {
    return [self initWithItemName:@"Item"];
}

- (void)setItemName:(NSString *)str {
    _itemName = str;
}

- (NSString *)itemName {
    return _itemName;
}

- (void)setSerialNumber:(NSString *)str {
    _serialNumber = str;
}

- (NSString *)serialNumber {
    return _serialNumber;
}

- (void)setValueInDollars:(int)v {
    _valueInDollars = v;
}

- (int)valueInDollars {
    return _valueInDollars;
}

- (NSDate *)dateCreated {
    return _dateCreated;
}

- (NSString *)description {
    NSString *descriptionString = [[NSString alloc] initWithFormat:@"Name: %@, Serial No: %@, Price: $%d, Date Created: %@", self.itemName, self.serialNumber, self.valueInDollars, self.dateCreated];
    return descriptionString;
}

@end
