//
//  MLHContainer.m
//  RandomItems
//
//  Created by Melanie H on 5/20/15.
//  Copyright (c) 2015 Melanie Hsu. All rights reserved.
//

#import "MLHContainer.h"

@implementation MLHContainer

/* the designated initializer */
- (instancetype) initWithName:(NSString *)name
                        value:(int)val {
    self = [super init];
    if (self) {
        _subitems = [[NSMutableArray alloc] init];
        _name = name;
        _value = val;
        _maxidx = 0;
    }
    return self;
}

/* if no value is given, provide an initial value of 0 */
- (instancetype) initWithName:(NSString *)name {
    return [self initWithName:name value:0];
}

/* override the inherited initializer */
- (instancetype) init{
    return [self initWithName:@"Box"];
}

/* accessor methods */
- (NSMutableArray *) subItems {
    return _subitems;
}

- (NSString *) name {
    return _name;
}

- (void) setName:(NSString *)name {
    _name = name;
}

- (int) value {
    return _value;
}

- (void) addValue:(int)val {
    _value += val;
}

- (int) maxIdx {
    return _maxidx;
}

- (void) setMaxIdx {
    _maxidx = _maxidx+1;
}

/* methods to add items to array */
- (void) addItem:(MLHItem *)item {
    self.subItems[self.maxIdx] = item;
    [self addValue:[item valueInDollars]];
    [self setMaxIdx]; //increment max index by 1
}

- (void) addItem:(MLHItem *)item
         toIndex:(int)idx {
    /* if the index is invalid, do nothing */
    if (idx >= 0 && idx < [self.subItems count]) {
        self.subItems[idx] = item;
    }
}

- (NSString *)description {
    NSMutableArray *contents = [[NSMutableArray alloc] init];
    for (int i = 0; i < [self maxIdx]; i++) {
        contents[i] = [[NSString alloc]initWithFormat:@"%@", self.subItems[i]];
    }
    NSString *descriptionString = [[NSString alloc]initWithFormat:@"Name: %@, Price: $%d, Contents: %@",
                                   self.name, self.value, contents];
    return descriptionString;
}

@end
