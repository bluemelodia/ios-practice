//
//  MLHContainer.h
//  RandomItems
//
//  Created by Melanie H on 5/20/15.
//  Copyright (c) 2015 Melanie Hsu. All rights reserved.
//

#import "MLHItem.h"

@interface MLHContainer : MLHItem {
    NSMutableArray *_subitems;
    NSString *_name;
    int _value;
    int _maxidx;
}

/* initializers */
- (instancetype) initWithName:(NSString *)name
                        value:(int)val;

- (instancetype) initWithName:(NSString *)name;

/* methods to add items to the array */
- (void) addItem:(MLHItem *)item;
- (void) addItem:(MLHItem *)item
         toIndex:(int)idx;

/* accessor methods */
- (NSMutableArray *) subItems;
//- (void) setSubItems;

- (NSString *) name;
- (void) setName:(NSString *)name;

- (int) value;
- (void) addValue:(int)val;

- (int) maxIdx;
- (void) setMaxIdx;

@end
