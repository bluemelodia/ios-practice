//
//  main.m
//  RandomItems
//
//  Created by Melanie H on 5/19/15.
//  Copyright (c) 2015 Melanie Hsu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MLHItem.h"
#import "MLHContainer.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        MLHContainer *box = [[MLHContainer alloc] initWithName:@"Present" value:50];
        if (box) {
            NSLog(@"Present successfully created.");
        }
        /* Send the message addObject: to the NSMutableArray pointed
         to by the variable array, passing an instance of a NSString each time */
        for (int i = 0; i < 10; i++) {
            [box addItem:[MLHItem randomItem]];
        }
        MLHContainer *inside = [[MLHContainer alloc] initWithName:@"Cookies" value:100];
        [inside addItem:[MLHItem randomItem]];
        [box addItem:inside];
        
        NSLog(@"%@", box);
        box = nil; //destroy the MLHContainer object
    }
    return 0;
}