//
//  MLHItem.h
//  RandomItems
//
//  Created by Melanie H on 5/19/15.
//  Copyright (c) 2015 Melanie Hsu. All rights reserved.
//

/* Making a new class: MLHItem, subclass of NSObject 
   BNRItem.h is the header file (interface file), declaring the name of the new class, 
   its superclass, the instance variables that each instance of the class has, and any
   methods that the class implements */

#import <Foundation/Foundation.h>

/* class declaration */
@interface MLHItem : NSObject {
}

/* initializers - start with the word "init"
   based on how much you know about the object, pick the appropriate initializer */

//a class method declaring a random item
+ (instancetype)randomItem;

//designated initializer for MLHItem
- (instancetype)initWithItemName:(NSString *)name
                  valueInDollars:(int)value
                    serialNumber:(NSString *)sNumber;

- (instancetype) initWithItemName:(NSString *)name;

@property (nonatomic, copy) NSString *itemName;
@property (nonatomic, copy) NSString *serialNumber;
@property (nonatomic) int valueInDollars;
@property (nonatomic, readonly, strong) NSDate *dateCreated;
@property (nonatomic, strong) MLHItem *containedItem;
@property (nonatomic, weak) MLHItem *container;

@end
