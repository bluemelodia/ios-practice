//
//  main.m
//  Quiz
//
//  Created by Melanie H on 5/15/15.
//  Copyright (c) 2015 Melanie Hsu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
