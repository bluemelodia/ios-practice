//
//  QuizViewController.m
//  Quiz
//
//  Created by Melanie H on 5/15/15.
//  Copyright (c) 2015 Melanie Hsu. All rights reserved.
//

#import "QuizViewController.h"

@interface QuizViewController ()

@property (nonatomic) int currentQuestionIndex;

/* pointers to arrays - ordered lists containing Q&A */
@property (nonatomic, copy) NSArray *questions;
@property (nonatomic, copy) NSArray *answers;

/* each instance of QuizViewController has an outlet named questionLabel,
a pointer to a UILabel object - the IBOutlet keyword tells Xcode that
you will set this outlet using Interface Builder */
@property (nonatomic, weak) IBOutlet UILabel *questionLabel;
@property (nonatomic, weak) IBOutlet UILabel *answerLabel;

@end

@implementation QuizViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/* When an instance of QuizViewController is created, it is sent the message
 initWithNibName:bundle: */
- (instancetype) initWithNibName: (NSString*)nibNameOrNil
                          bundle: (NSBundle*)nibBundleOrNil {
    /* Call the init method implemented by the superclass */
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if (self) {
        /* create two arrays filled with questions and answers
        and make the pointers point to them */
        self.questions = @[@"Which sorting method is more time-efficient on average, quick sort or bubble sort?",
                           @"Which cells in the body are targeted by measles?",
                           @"Which programming language is more suitable for back-end, C or Python?"
                           ];
        self.answers = @[@"Quick sort (nlog(n)), bubble sort is n^2.",
                         @"T and B cells, macrophages, and dendritic cells",
                         @"C"
                         ];
    }
    //return the address of the new object
    return self;
}

/* IBAction keyword tells Xcode that you will be making the connection 
 in Interface Builder */
- (IBAction) showQuestion:(id)sender {
    // Step to the next question
    self.currentQuestionIndex++;
    
    //Am I past the last question?
    if (self.currentQuestionIndex == [self.questions count]) {
        //Go back to the first question
        self.currentQuestionIndex = 0;
    }
    
    //Get the string at the index in the questions array
    NSString *question = self.questions[self.currentQuestionIndex];
    
    //Display the string in the question label
    self.questionLabel.text = question;
    
    //Reset the answer label
    self.answerLabel.text = @"???";
}

- (IBAction) showAnswer:(id)sender {
    //What is the answer to the current question?
    NSString *answer = self.answers[self.currentQuestionIndex];
    
    //Display it in the answer label
    self.answerLabel.text = answer;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
