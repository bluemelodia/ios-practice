Directory Guide
1) Quiz: A simple Q&A app.
2) RandomItems: Created the MLHItem class that allows the user to create and manipulate instances of real-world objects. The project includes a tester class that creates and logs an array of MLHItems.
	Bronze Challenge: Bug testing for out of array index. Completed 5/19/15.
	Silver Challenge: Created additional initializer that takes the itemName and serialNumber. Completed 5/19/15.
	Gold Challenge: Created MLHContainer, a subclass of MLHItem that is able to hold both MLHContainer and MLHItem objects. Completed 5/20/15.
3) 
